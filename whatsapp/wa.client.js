const { Client, LocalAuth } = require("whatsapp-web.js");
const { waSendMessage, waFormatMessage, waMinify } = require("./wa.helpers.js");

const waConnect = (id, socket) => {
    try {
        const client = new Client({
            puppeteer: {
                headless: false,
            },
            authStrategy: new LocalAuth({
                clientId: id,
            }),
        });

        console.log("connecting");
        socket.emit("wa:status", { status: "connecting", data: {} });

        client.on("qr", (qr) => {
            console.log("qr");
            socket.emit("wa:qr", qr);
        });
        client.on("ready", async () => {
            console.log("ready");
            const user = await client.getContactById(
                client?.info?.me?._serialized
            );

            user.profilePic = await client.getProfilePicUrl(
                client?.info?.me?._serialized
            );
            socket.emit("wa:status", { status: "ready", data: { user } });
        });
        client.on("auth_failure", (message) => {
            console.log("auth_failure");
            socket.emit("wa:authenticated", {
                status: false,
                message: message,
            });
        });
        client.on("authenticated", () => {
            console.log("authenticated");
            socket.emit("wa:authenticated", {
                status: true,
                message: "authenticated",
            });
        });
        client.on("disconnected", (message) => {
            console.log("disconnected", message);
            socket.emit("wa:status", {
                status: "disconnected",
                data: { message },
            });
        });
        client.on("message", (message) => {
            console.log("message");
            socket.emit("wa:message", message);
        });
        client.on("message_create", (message) => {
            console.log("message_create");
            socket.emit("wa:message_create", message);
        });

        client.initialize().catch((e) => console.log(e));

        socket.on("wa:send_message", (message) => {
            waSendMessage(client, message);
        });

        socket.on("disconnecting", (message) => {
            console.log("disconnecting");
            client.destroy();
        });
    } catch (e) {
        console.log(e);
    }
};

module.exports = { waConnect };
