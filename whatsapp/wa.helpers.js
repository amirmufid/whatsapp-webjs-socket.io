const { MessageMedia } = require("whatsapp-web.js");
const waFormatMessage = (text) => {
    const formated = text
        .replaceAll("<p>", "")
        .replaceAll("</p>", "\n\n")
        .replaceAll("<strong>", "*")
        .replaceAll("</strong>", "*")
        .replaceAll("<em>", "_")
        .replaceAll("</em>", "_")
        .replaceAll("&nbsp;", " ")
        .replaceAll("<br>", "\n")
        .replaceAll("<br/>", "\n")
        .replaceAll("&amp;", "&")
        .replaceAll("&lt;", "<")
        .replaceAll("&gt;", ">")
        .replaceAll("&quot;", '"')
        .replaceAll("&#039;", "'");
    return formated;
};

const waMinify = (s) => {
    return s
        ? s
              .replace(/\>[\r\n ]+\</g, "><") // Removes new lines and irrelevant spaces which might affect layout, and are better gone
              .replace(/(<.*?>)|\s+/g, (m, $1) => ($1 ? $1 : " "))
              .trim()
        : "";
};

const waSendMessage = async (client, param) => {
    try {
        let phoneNumber = param.phone_number;
        let message = waFormatMessage(waMinify(param.message));
        let image = param.image;

        if (phoneNumber.startsWith("0")) {
            phoneNumber = `62${phoneNumber.slice(1)}@c.us`;
        } else if (phoneNumber.startsWith("62")) {
            phoneNumber = `${phoneNumber}@c.us`;
        } else {
            phoneNumber = `62${phoneNumber}@c.us`;
        }

        const checkPhoneNumber = await client.isRegisteredUser(phoneNumber);

        const media = await MessageMedia.fromUrl(image);
        if (checkPhoneNumber) {
            client.sendMessage(phoneNumber, message, {
                media,
                linkPreview: true,
            });

            return {
                status: true,
                phone_number: phoneNumber,
                message: message,
            };
        } else {
            return {
                status: false,
                phone_number: phoneNumber,
                message: `${phoneNumber} not registered in whatsapp`,
            };
        }
    } catch (e) {
        console.log(e);
    }
};

module.exports = {
    waFormatMessage,
    waMinify,
    waSendMessage,
};
