const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const http = require("http");
const server = http.createServer(app);
const { Server } = require("socket.io");
const { waConnect } = require("./whatsapp/wa.client");
const io = new Server(server);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json({ limit: "20mb" }));

io.on("connection", (socket) => {
    console.log("socket", "connect");
    socket.on("wa:create_session", (message) => {
        console.log("wa:create_session", message);
        waConnect(message.id, socket);
    });
});

server.listen(1000, () => {
    console.log("listening on *:1000");
});
